import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import BookList from '../components/BookList.vue'
Vue.use(VueRouter)
const routes = [
    {
      path: "/",
      name: "Home",
      component: Home,
    },
    {
      path: "/book/:id",
      name: "Book",
      component: () =>
                import(/* webpackChunkName: "bookdetail" */ "../views/BookDetail.vue"),
    },
    {
      path: "/favoritebook",
      name: "Favorite",
      component: () =>
                import(/* webpackChunkName: "favoritebook" */ "../views/FavoriteBook.vue"),
    },
    {
      path: '/search',
      component: BookList,
      props: route => ({ query: route.query.q })
    }
]
const router = new VueRouter({
    routes,
});


export default router;