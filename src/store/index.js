import Vue from "vue";
import Vuex from "vuex";
import book from '@/store/book.module';

Vue.use(Vuex);

export default new Vuex.Store({
  
  modules: {
    book
  },
});