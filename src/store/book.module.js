import axios from 'axios';
import _ from 'lodash';
export default {
    state: {
        booklist:[],
        userFavoriteBook:JSON.parse(localStorage.getItem("favoritebook")),
        bookdetail:''
    },
    getters:{
        getListBook: (state) =>{
            return state.booklist
        },
        getFavoriteBook: (state) =>{
            return state.userFavoriteBook
        },
        getBookDetailById: (state) =>{
            return state.bookdetail
        }
    },
    mutations:{
        setSearchBook(state, data){
            state.booklist = data.items
        },
        setFavoriteBook(state){
            
            state.userFavoriteBook = JSON.parse(localStorage.getItem("favoritebook"))
        },
        setBookDetail(state,data){
            state.bookdetail = data
        }
    },
    actions:{
        fetchSearchBook({commit}, payload){
            return new Promise((resolve, reject) => {
                
                let filtersopt = '';
                
                _.each( payload.filters, function( filter ){
                    
                    if(filter === 'ebooks' ){
                        if(payload.saleability.length == 1){
                            _.each( payload.saleability, function( sale){
                                if(sale === 'Free'){
                                    filtersopt = '&filter=free-ebooks'
                                }
                                else if(sale === "Paid"){
                                    filtersopt = '&filter=paid-ebooks'
                                }
                            })
                        }
                        else{
                            filtersopt = '&filter=ebooks'
                        }
                        
                    }
                    if(filter === 'non ebook'){
                        filtersopt = '&printType=magazines'
                    }
                    
                });
               
              
                const config = {
                    method: "get",
                    url: `https://www.googleapis.com/books/v1/volumes?q=${payload.searchword?payload.searchword:''}+intitle${filtersopt}`,
                };
                
                axios(config)
                .then((response) => {
                    commit('setSearchBook', response.data)
                    resolve("success");
                })
                .catch((response) => {
                    reject(response);
                });
            });
        },
        addFavoriteBooks({commit},payload){
            return new Promise((resolve, reject) =>{
                try{
                    
                    let userfav = JSON.parse(localStorage.getItem("favoritebook") || "[]")
                    let favoritebook = {
                        bookid: payload.bookid,
                        booktitle: payload.booktitle,
                        bookcoverimage: payload.bookcoverimage
                    }
                    userfav.push(favoritebook)
                    //userfav = _.uniqWith(userfav, _.isEqual)
                    userfav = _.uniqBy(userfav, 'bookid')
                    if(payload.favorite == false){
                        _.remove(userfav, function(e) {
                            return e.bookid === payload.bookid
                        });
                    }
                    
                    localStorage.setItem("favoritebook",JSON.stringify(userfav))
                    commit("setFavoriteBook")
                    resolve("success")
                }
                catch(error){
                    reject(error)
                }

            });
        },
        fetchBookById({commit},payload){
            return new Promise((resolve, reject) =>{
                const config = {
                    method: "get",
                    url: `https://www.googleapis.com/books/v1/volumes/${payload.bookid}`,
                };
                
                axios(config)
                .then((response) => {
                    commit('setBookDetail', response.data)
                    resolve("success");
                })
                .catch((response) => {
                    reject(response);
                });
            });

        }
    }
}
